# OCP
# razred 'Server' je otvoren za extenzije ali zatvoren za modifikacije
class Server():
    _users = {'admin@admin':{'name':'admin','password':'pass'}} 

    def __init__(self):
        pass

    def register(self,email,name,password):
        if email in self._users.keys():
            return False 
        else:
            self._users[email] = {'name':name,'password':password}
            return True
    def login(self,email,password):
        if self._users[email]['password'] == password:
            return True 
        else:
            return  False
    


class AdminServer(Server):
    def adminLogin(self,email,password): #admin login dodana extenzija na razred
        if email=='admin@secure.mail' and password == 'Pa$$w0rd':
            return True
        else:
            return False
